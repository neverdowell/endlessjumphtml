var BAR_BORDER = 0;
var BAR_BORDER_COLOR = "rgb(0,0,0)";
var BAR_TYPE_1 = 0;
var BAR_TYPE_2 = 1;
var BAR_TYPE_3 = 2;
var BAR_TYPE_4 = 3;
var BAR_TYPE_5 = 4;
//colors in array: light gray, green, blue, violet, red
var BAR_TYPE_COLOR = new Array("rgb(150,150,150)","rgb(40,230,0)","rgb(40,150,255)","rgb(200,0,200)","rgb(255,50,50)");						
var BAR_TYPE_MODIFIER = new Array(1,2,3,5,8);
var BAR_TYPE_PROBABILTY = new Array(1.0,0.5,0.25,0.125,0.0625);
var BAR_HEIGHT = 10;

function Bar(x,y, barType){
	this.x = x;
	this.y = y;
	if (barType === -1){
		this.barType = BAR_TYPE_1;
		this.isDestroyable = false;
		this.color = "rgb(0,0,0)";
		this.stars = 1; 
	} else {
		this.barType = barType;
		this.isDestroyable = true;
		this.color = BAR_TYPE_COLOR[barType];
		this.stars = BAR_TYPE_MODIFIER[barType];
	}
	this.width = (CANVAS_WIDTH / 5) / (this.barType + 1);
	this.height = BAR_HEIGHT;
	
	// add functions
	this.draw = drawBar;
}

function drawBar(){
	if (this.y > 0 - this.height){
		canvasContext.fillStyle = this.color;
		canvasContext.fillRect(this.x,this.y,(CANVAS_WIDTH / 5) / (this.barType + 1),this.height);
		canvasContext.fillStype = BAR_BORDER_COLOR;
		canvasContext.strokeRect(this.x,this.y,(CANVAS_WIDTH / 5) / (this.barType + 1),this.height);
	}
}


function setBarType(barType){
	//this.width = 100 / (barType + 1);
	//this.color = BAR_TYPE_COLOR[barType];
	//this.stars = BAR_TYPE_MODIFIER[barType];
}