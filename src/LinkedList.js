/* LinkedList is multi purpose structure.
 * List features: insertAt(i), get(i), removeAt(i)
 * Stack features: push, pop
 * Queue features: enqueue, dequeue (adds/removes first element)
 */

function LinkedList() {
    this._length = 0;
    this._head = null;
    this._last = null; // add to all functions!
}

LinkedList.prototype = {
		//queue methods
		enqueue: function (data){
			//create a new node, place data in
	        var node = {
	                data: data,
	                next: null
	            },
	        oldHead = this._head;    
	        this._head = node;
	        node.next = oldHead;
	        this._length++;
		},
		
	    dequeue: function (){
	    	current = this._head;
	    	this._head = current.next;
    		return current.data;
	    },
	    
	    // stack methods
	    push: function (data){

	        //create a new node, place data in
	        var node = {
	                data: data,
	                next: null
	            },

	            //used to traverse the structure
	            current;

	        //special case: no items in the list yet
	        if (this._head === null){
	            this._head = node;
	        } else {
	            current = this._head;

	            while(current.next){
	                current = current.next;
	            }

	            current.next = node;
	        }

	        //don't forget to update the count
	        this._length++;

	    },

	    pop: function(){
	    	if (this._length === 0) return null;
	    	
	        var current = this._head;
	        var previous = null;

            //find the right location
            while(current._next){
                previous = current;
                current = current.next;
            }
            this._length--;
            previous._next = null;
	        return current.data;            
	    },
	    
	    // list methods
	    get: function(index){

	        //check for out-of-bounds values
	        if (index > -1 && index < this._length){
	            var current = this._head,
	                i = 0;

	            while(i++ < index){
	                current = current.next;
	            }

	            return current.data;
	        } else {
	            return null;
	        }
	    },
   
	    insertAt: function(index, data){
	    	if (data && index > -1 && index < this._length)
	    		var node = {
	                data: data,
	                next: null
	            };
	            var current = this._head;
	            var previous;
	            var i = 0;
                //find the right location
                while(i++ < index){
                    previous = current;
                    current = current.next;
                }
                this._length++;
                node.next = current;
                if (i == 0){
                	this._head = node;
                } else {
                	previous.next = node;
                }
	    },
	    
	    removeAt: function(index){

	        //check for out-of-bounds values
	        if (index > -1 && index < this._length){

	            var current = this._head,
	                previous,
	                i = 0;

	            //special case: removing first item
	            if (index === 0){
	                this._head = current.next;
	            } else {

	                //find the right location
	                while(i++ < index){
	                    previous = current;
	                    current = current.next;
	                }

	                //skip over the item to remove
	                previous.next = current.next;
	            }

	            //decrement the length
	            this._length--;

	            //return the value
	            return current.data;            

	        } else {
	            return null;
	        }
	    },
	    
	    length: function(){
	    	return this._length;
	    }
};