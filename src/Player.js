function Player (canvasContext, x, y, width, height, gravity, color) {
	this.canvasContext = canvasContext;
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.verticalAcceleration = 1;
	this.horizontalAcceleration = 0;
	this.horizontalMaxAcceleration = 200;
	this.gravity = gravity;
	this.color = color;
	this.verticalSpeed = 15;
	this.maxVelocity = 200;
	this.jumpSpeed = 200;
	this.jumpMaxDuration = 400;
	this.jumpDuration = this.jumpMaxDuration;
	this.isFalling = true;
	this.stars = 0;
	this.points = 0;
	// add functions
	this.update = updatePlayer;
	this.draw = drawPlayer;
	this.moveRight = movePlayerRight;
	this.moveLeft = movePlayerLeft;
	this.collides = playerCollides;
	
};


function updatePlayer(deltaTime){
	// update vertical movement
	if (this.isFalling){
		this.verticalAcceleration += this.gravity;
		if (this.verticalAcceleration > this.maxVelocity) this.verticalAcceleration = this.maxVelocity; // stop verticalAcceleration growing
		/*if (this.y > CANVAS_HEIGHT){
			this.y = 100;
		}*/
	} else {
		this.verticalAcceleration = -this.jumpSpeed;
		this.jumpDuration -= deltaTime;
		if (this.jumpDuration < 0){
			this.isFalling = true;
			this.jumpDuration = this.jumpMaxDuration;
		}
	}
	// update horizontal movement
	if (this.horizontalAcceleration > 2){
		this.horizontalAcceleration -= this.verticalSpeed /3;
	} else if (this.horizontalAcceleration < -2){
		this.horizontalAcceleration +=  this.verticalSpeed /3;
	} else {
		this.horizontalAcceleration = 0;
	}
	if (this.horizontalAcceleration > this.horizontalMaxAcceleration) this.horizontalAcceleration = this.horizontalMaxAcceleration;
	if (this.horizontalAcceleration < -this.horizontalMaxAcceleration) this.horizontalAcceleration = -this.horizontalMaxAcceleration;
	this.x += deltaTime / 1000 * this.horizontalAcceleration;
	this.x = inRange(this.x, 0, CANVAS_WIDTH - this.width);
	this.y += deltaTime / 1000 * this.verticalAcceleration;
};


// draw player
function drawPlayer(){
	canvasContext.fillStyle = this.color;
	canvasContext.fillRect(this.x,this.y,this.width,this.height);
};


function movePlayerRight(deltaTime){
	this.horizontalAcceleration += this.verticalSpeed;
};


function movePlayerLeft(deltaTime){
	this.horizontalAcceleration -= this.verticalSpeed;
};


function playerCollides(bar){
	if (this.isFalling){
		if (this.verticalAcceleration > 0){
			if (isBetween(this.y + this.height, bar.y, bar.y + bar.height)){
				// check if one of player's corners is between vertical start and end of bar -> will bounce off
				if (isBetween(this.x, bar.x, bar.x + bar.width) | isBetween(this.x + this.width, bar.x, bar.x + bar.width)){
					this.y = bar.y - this.height - 1;
					this.isFalling = false;
					if (bar.isDestroyable) return true;
				}
			}
		}
	}
	return false;
}

function isBetween(value, min, max){
	if (value >= min){
		if (value <= max){
			return true;
		}
	}
	return false;
};


function inRange(value, min, max){
	if (value < min) return min;
	if (value > max) return max;
	return value;
};

