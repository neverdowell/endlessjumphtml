// writes text to document. do not mix with canvas code -> will end up ugly, 
// because canvas is somehow not drawn, when document.writeln(...) is called. Dunno why yet.
function out(text){
  document.writeln(text);
};

//return browser information like application name, browser type and version as String
function browserInfo(){
	return navigator.appName + "\n" + navigator.userAgent + "\n" + navigator.appVersion + "\n";
};

function clamp (min, max) {
	  return Math.min(Math.max(this, min), max);
};