// level stuff
var currentLevel = 0;
var levelLimits = [500,1500,3000,6000, 9000, 1000000000];
var levelText = ["Level 0: Let's jump. Move LEFT and RIGHT.", 
                "Level 1: That's really simple, isn't it?", 
                "Level 2: Reach 3000 Points for next level", 
                "Level 3: Stop playing - solve your Tasks", 
                "Level 4: Seriously, SOLVE YOUR TASKS!",
                "Level Cheater: Not possible to reach this many points :)"];
var time = 0;

function Level(){
	// functions
	this.draw = drawLevel;
	this.update = updateLevel;
}

function drawLevel(){
	// draw Level info
	canvasContext.fillStyle = "#FFFFFF"; 
	canvasContext.fillRect(0,CANVAS_HEIGHT - 20,CANVAS_WIDTH, 100);
	canvasContext.fillStyle = "#000000";
	canvasContext.strokeRect(0,CANVAS_HEIGHT - 20, CANVAS_WIDTH, CANVAS_HEIGHT);
	canvasContext.fillText(levelText[currentLevel], 10, CANVAS_HEIGHT - 7);
}

function updateLevel(){
	if (player.points > levelLimits[currentLevel + 1]){
		currentLevel++;
	}
 
}
