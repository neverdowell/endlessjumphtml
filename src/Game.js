// constants
var CANVAS_WIDTH = 500;
var CANVAS_HEIGHT = 500;
var FPS = 50;
var COOKIE_NAME = "endlessjump";

// game vars
var canvasContext;
var gameCanvas;
var player;
var bar;
var isAlive = true;
var isPaused = false;
var wasPaused = false;
var wasSpacePressed = false;
var oldTime = new Date().getTime();
var currentTime = 0;
var deltaTime = 0;
var minDeltaTime = 10000;
var maxDeltaTime = -10000;
var barList = new LinkedList();
var currentMaxBarY = CANVAS_HEIGHT - 50;
var highScore = new LinkedList();
var highScoreXOffset = 10;
var highScoreYOffset = 10;

// level vars
var keysEnabled = true;
var barsEnabled = true;
var useCrazyMode = false;
var invertedMode = false;
var level = new Level();

// inits game
function init(){
	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame; 
	gameCanvas = document.getElementById("gameCanvas");									// get canvas element
	setCanvasWidth(CANVAS_WIDTH);
	setCanvasHeight(CANVAS_HEIGHT);
	if (gameCanvas.getContext("2d")) {
		canvasContext = gameCanvas.getContext("2d");									// get context for drawing etc.
		if (canvasContext) {
			setInterval(gameLoop, 1000/FPS);											// callback of gameLoop in milliseconds
			//window.requestAnimationFrame(gameLoop);
		} else {
			out("Could not get 2d context from canvas");
		}
	} else {
		document.writeln("Could not find element \"gameCanvas\"");
	}
	resetGame();
}

function resetGame(){
	currentMaxBarY = CANVAS_HEIGHT - 50;
	player = new Player(canvasContext,CANVAS_WIDTH / 2,currentMaxBarY - 30,20,35,10,"#000000");					// create player
	barList = new LinkedList();
	barList.push(new Bar(CANVAS_WIDTH / 2 - 50, currentMaxBarY, -1));
	isAlive = true;
	isPaused = false;
	// level stuff
	CANVAS_WIDTH = 500;
	CANVAS_HEIGHT = 500;
}

// game loop
function gameLoop(){
	update();
	draw();
}

// update game logic
function update(){
	currentTime = new Date().getTime();													// compute deltatime
	deltaTime = currentTime - oldTime;
	minDeltaTime = Math.min(minDeltaTime, deltaTime);
	maxDeltaTime = Math.max(maxDeltaTime, deltaTime);
	if (isAlive){
		if (player.y > CANVAS_HEIGHT) {
			isAlive = false;
			addScore();
		}
		if (!isPaused){																		// is paused disable input except 'space' for resuming
			player.update(deltaTime);														// update player
		    if (keysEnabled){
				if(keydown.left){
			    	player.moveLeft(deltaTime);
			    }
			    if (keydown.right){
			    	player.moveRight(deltaTime);
			    }
		    }
		}
		if (keydown.space){
	    	if (!this.wasSpacePressed){
	    		this.isPaused = !this.isPaused;
	    		this.wasSpacePressed = true;
	    	}
	    }  else {
	    	this.wasSpacePressed = false;
	    }
		
	    //window.requestAnimationFrame(gameLoop);
	    for (var i = 0; i < barList.length(); i++){											// check collision with bars
	    	var currentBar = barList.get(i);
			if (player.collides(currentBar)){
				player.stars += currentBar.stars;
				player.points += currentBar.stars * 2;
				barList.removeAt(i);
				break;
			};
		}
	    // scroll player and bars if necessary
	    var tempScrollAmount = - deltaTime / 1000 * player.verticalAcceleration;
	    if (tempScrollAmount > 0){
		    if (player.y < 50){
		    	scrollDown(tempScrollAmount);
		    } else if (player.y < 100){
		    	scrollDown(tempScrollAmount / 2);
		    } else if (player.y < 150){
		    	scrollDown(tempScrollAmount / 3);
		    }
	    }
	    // create new bars if necessary
	    if (barsEnabled) createNewBar();
	} else { // update dead state
		if (keydown.space){
	    	if (!this.wasSpacePressed){
	    		this.wasSpacePressed = true;
	    		isAlive = true;
                        resetGame();
                        currentLevel = 0; 
	    	}
	    }  else {
	    	this.wasSpacePressed = false;
	    }
	}
    
    if (keydown.esc){
    	resetGame();
    }
    level.update(player.points);
    oldTime = currentTime;																// remember current time as old time for next deltaTime computation
}

// draw game
function draw(){
	if (isAlive){
		if (isPaused){
			if (!wasPaused) {// draw paused state
				canvasContext.fillStyle = "rgba(255,0,0,0.8)"; 
				canvasContext.alpha = 0.5;
				canvasContext.fillRect(0,CANVAS_HEIGHT / 2 - 50,CANVAS_WIDTH, 100);
				canvasContext.fillStyle = "#000000";
				canvasContext.fillText("PAUSED", CANVAS_WIDTH / 2 - 20, CANVAS_HEIGHT / 2 + 5);
			}
		} else {
			if (useCrazyMode){
				canvasContext.fillStyle = "rgba(255, 255, 255, 0.1)";
				canvasContext.fillRect(0,0,CANVAS_WIDTH,CANVAS_HEIGHT);
			} else {
				canvasContext.clearRect(0,0,CANVAS_WIDTH,CANVAS_HEIGHT);							// clear canvas
			}
			
			for (var i = 0; i < barList.length(); i++){
				barList.get(i).draw();
			}
			player.draw();
			canvasContext.fillText("Punkte:" + player.stars, 5, 10);
			canvasContext.fillText("Hoehe:" + player.points, 5, 25);

		}
	    
	} else { // draw dead state
		canvasContext.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
                canvasContext.fillText("Game Over.", 220, 200);
                canvasContext.fillText("Presse Leertaste zum Neustart.", 180, 230);
	}

	// draw level
	level.draw();
    wasPaused = isPaused;
}
 

// resize width of canvas at runtime
function setCanvasWidth(width){
	CANVAS_WIDTH = width;
	gameCanvas.setAttribute("width", width);
}

//resize height of canvas at runtime
function setCanvasHeight(height){
	CANVAS_HEIGHT = height;
	gameCanvas.setAttribute("height", height);
}

// get width of canvas
function getCanvasWidth(){
	return CANVAS_WIDTH;
}

// get height if canvas
function getCanvasHeight(){
	return CANVAS_HEIGHT;
}

function scrollDown(scrollAmount){
	player.y += scrollAmount;
	for (var i = 0; i < barList.length(); i++){
		var bar = barList.get(i);
		bar.y += scrollAmount;
		if (bar.y > CANVAS_HEIGHT){
			barList.removeAt(i);
		}
	}
	currentMaxBarY += scrollAmount;
	player.points += Math.ceil(scrollAmount);
}


function createNewBar(){
	while (currentMaxBarY > 0){
		var newBarTypeValue = Math.random() - player.points / 10000;
		var newBarType = 0;
		for (var i = BAR_TYPE_PROBABILTY.length - 1; i > 0; i--){
			if (newBarTypeValue < BAR_TYPE_PROBABILTY[i]){
				newBarType = i;
				break;
			}
		}
		var newX = Math.random() * (CANVAS_WIDTH - 100 / (newBarType + 1));
		currentMaxBarY -= 11 + Math.random() * (20 + player.stars/10);
		barList.push(new Bar(newX, currentMaxBarY, newBarType));
	}
}
