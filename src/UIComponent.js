// used for holding state and image index
var UICOMPONENT_NORMAL = 0;
var UICOMPONENT_ROLL_OVER = 1;
var UICOMPONENT_ACTIVATED = 2;
var UICOMPONENT_DISABLED = 3;


function UIComponent(x,y,width,height,images,callbackFunction){
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.images = images;
	this.callbackFunction = callbackfunction;
	this.enabled = true;
	this.isIn = false;
	this.isActivated = false;
	this.isVisible = true;
	//functions
	this.update = updateUIComponent;
	this.draw = drawUIComponent;
}

function updateUIComponent(x,y, isClicked){
	if (!isEnabled || !isVisible) return;
	if (x >= this.x && x < this.x + this.width && y >= this.y && y <= this.y + this.height){
		this.isIn = true;
		if (isClicked) {
			this.isActivated = !this.isActivated;
		}
	} else {
		this.isIn = false;
	}
}

function drawUIComponent(){
	if (!this.isVisible) return;
	if (!this.isEnabled) {
		// draw disabled;
	} else if (this.isIn){ // enabled && rollover
		// draw rollover
	} else if (this.isActivated){
		// draw activated
	} else {
		// draw normal
	} 

}